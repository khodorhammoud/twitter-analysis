const express = require('express');
const router = express.Router();
const TwitterFunctions = require('../js/models/twitter-functions')
const TwitterAPICalls = require('../js/models/twitter-api-call-manager')



/*
 * Get profile data
 * Requires either a user id or a screen name
 * Return a list of profile data described in twitter-functions::Profile
 */
router.get('/users/profile/:id', function(req, res, next) {
	TwitterFunctions.get_profile_info(req.params, req, res, next);
});



/*
 * Get influencial followers
 * Requires either a user id or a screen name
 */
router.get('/users/followers-influencial/:id', function(req, res, next) {
	TwitterFunctions.get_influencial_followers(req.params, req, res, next);
});



/*
 * Get tweet info
 * Requires tweet id
 */
router.get('/tweets/tweet/:id', function(req, res, next) {
	TwitterFunctions.get_tweet(req.params, req, res, next);
});

router.get('/tests/test1',function(req,res,next){
	TwitterFunctions.getFollowersFromTwitter({id:'mkbhd'},res);
	/*TwitterAPICalls.callAPI("statuses/user_timeline", {screen_name:'mkbhd',count:1}).then(function(para){
		console.log(para);
		res.send(para);
	});*/
});

router.get('/followers_graph/:id',function(req, res, next){
	TwitterFunctions.getFollowersGraphData(req.params.id,res);
});






















//
//  search twitter for all tweets containing the word 'banana' since July 11, 2011
//
router.get('/banana',function(req, res, next){
	T.get('search/tweets', { q: 'banana since:2011-07-11', count: 100 }, function(err, data, response) {
		texts = [];
		data['statuses'].forEach(function (item, index) {
			texts.push(`<p>${item.text}</p>`);
		});
		res.send(texts.join());
	});
});


//
//  get the list of user id's that follow @tolga_tezel
//
router.get('/followers',function(req, res, next){
	T.get('followers/ids', { screen_name: 'mkbhd', cursor:"1663954241294200186",stringify_ids:true },  function (err, data, response) {
		// ids = [];
		// data['statuses'].forEach(function (item, index) {
		// 	texts.push(`<p>${item}</p>`);
		// });
		res.json(data);
	});
});


//
//  get the list of retweets
//
router.get('/followers/:id',function(req, res, next){
	T.get('statuses/retweets', { id: req.params.id, count: 100 },  function (err, data, response) {
		// ids = [];
		// data['statuses'].forEach(function (item, index) {
		// 	texts.push(`<p>${item}</p>`);
		// });
		res.json(data);
	});
});



/*// get all tasks
router.get('/tasks',function(req, res, next){
	db.tasks.find(function(err, tasks){
		if(err){
			res.send(err);
		}
		res.json(tasks);
	});
});
*/

module.exports = router;