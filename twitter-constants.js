/* Fields for fetching profile information { key: Label }*/
const Profile = {
	// Profile information
	"profile": {
		"name": "Screen name",
		"screen_name": "Twitter handle",
		"profile_image_url_https": "Profile photo",
		"verified": "Verified",
		"description": "Slogan",
		"location": "Location",
		"created_at": "Joined Date"
		/*
			These cannot be extracted
			profession, nationality
		*/
		/*
			These are not clear
			flags, hashtags, mentions, links
		*/
	},
	// Activity information
	"activity": {
		"created_at": "Last tweet date"
		/*
			Last retweet date
			Last reply date
			Require access to Account Activity API, which is not available for free subscription
		*/
	}
}

exports.get_profile_info = (last_retweet_data) => {
	user = last_retweet_data.user;
	result = {};
	for(key in Object.keys(Profile.profile))
		result[Profile.profile[key]] = user[key];

	for(key in Object.keys(Profile.activity))
		result[Profile.activity[key]] = last_retweet_data[key];
	return result;
}