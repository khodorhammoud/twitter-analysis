const Twit = require('twit');
var request = require('request');
const Lodash = require('lodash');
const mongojs = require('mongojs');
const fs = require('fs');

// const requiredFields = require

const TAPI = new Twit({
  consumer_key:         'jelb0ow21PKJBPucsRQ6nVYms',
  consumer_secret:      'qhjU3nUojX8lsPBjTmdyiHGqsnOUUR9S4CqrJx2SHSSJ2xTEul',
  access_token:         '1862900484-MtTTj2z3Pu8P9Uk5gKpKzkulwdq0R43167S1hxe',
  access_token_secret:  'CizqTW1QmCF837Hka5jVxCNo2RRD5LXuRvtTGNQ8wa7J3',
  timeout_ms:           60*1000,  // optional HTTP request timeout to apply to all requests.
});

var db = mongojs('admin', ['users', 'followers']);

/* Fields for fetching profile information { key: Label }*/
/*
	---Require access to Account Activity API, which is not available for free subscription---
		Last retweet date
		Last reply date

	---cannot be extracted---
		profession, nationality
		
	---not clear---
		flags, hashtags, mentions, links
*/
const Profile = {
	// Profile information
	profile: {
		name: "Screen name",
		screen_name: "Twitter handle",
		profile_image_url_https: "Profile photo",
		verified: "Verified",
		description: "Slogan",
		location: "Location",
		created_at: "Joined Date"
	},
	// Activity information
	activity: {
		created_at: "Last tweet date"
	}
}

const TwitterResourceURL = {
	userTimeline: "statuses/user_timeline",
	userFollowersIds: "followers/ids",
	userLookup: "users/lookup",
	singleTweet: "statuses/show",
}

/*
 * Handles the errors returned from twitter requests
 */
function handleTwitterReplyError(req, res, err) {
	if (err)
		res.send(err);
}

/*
 * Handles extracting user_id/screen_name from parameters
 */
function handleUserIdParse(params) {
	if ( isNaN(params.id) )
		return { screen_name: params.id };
	return { user_id: params.id };
}


/************************************************
*												*
 ========= Task: Get the profile data ==========
*												*
*************************************************/
function parseProfileInfo(last_retweet_data) {
	user = last_retweet_data.user;
	result = {};
	for(const key of Object.keys(Profile.profile)) {
		result[key] = {
			"label": Profile.profile[key],
			"value": user[key]
		};
	}

	for(const key of Object.keys(Profile.activity)) {
		result[key] = {
			"label": Profile.activity[key],
			"value": last_retweet_data[key]
		};
	}
	return result;
}

exports.get_profile_info = (params, req, res, next) => {
	_params = handleUserIdParse(params);
	_params.count = 1;
	_params.trim_user = false;
	_params.exclude_replies = true;
	_params.include_rts	= false;

	// Call the twitter API
	TAPI.get(TwitterResourceURL.userTimeline, _params, function(err, data, response) {
		handleTwitterReplyError(req, res, err);
		res.json(parseProfileInfo(data[0]));
	});
}


/************************************************
*												*
 ==== Task: Get the influencial followers ======
*												*
*************************************************/
var FOLLOWERS_IDS = [];
var FOLLOWERS_DATA = [];
var FINISHED_FETCHING_FOLLOWERS = false;
const IDS_COUNT_PER_FETCH = 10; //max value is 5000
const FOLLOWERS_INFO_COUNT_PER_FETCH = 5; //max value is 100
const MAX_FOLLOWERS_COUNT = 20; //set the meximum number of followers to fetch
var IDS_COUNT = 0;
var followers_req, followers_res, followers_param;
const FOLLOWERS_COUNT_THRESHOLD = 1;// 1000

function filterInfluencialUsers(data) {
	influencial_users = [];
	for(user of data) {
		// checking account activity omitted due to api constraints
		tmp = {}
		if(user.followers_count && user.followers_count>FOLLOWERS_COUNT_THRESHOLD) {
			for(const key of Object.keys(Profile.profile)) {
				tmp[key] = {
					"label": Profile.profile[key],
					"value": user[key]
				};
			}
			influencial_users.push(tmp);
		}
	}
	return influencial_users;
}

function fetchFollowersInfo() {
	ids = FOLLOWERS_IDS.splice(-1*FOLLOWERS_INFO_COUNT_PER_FETCH);
	console.log(ids);
	if(ids.length>0){
		TAPI.post(TwitterResourceURL.userLookup, {user_id: ids}, function(err, data, response) {
			handleTwitterReplyError(followers_req, followers_res, err);
			FOLLOWERS_DATA = FOLLOWERS_DATA.concat(filterInfluencialUsers(data));
			fetchFollowersInfo();
		});
	} else { //Finished fetching followers
		followers_res.json(FOLLOWERS_DATA);
		return;
	}
}

function addFollowersIds(ids) {
	FOLLOWERS_IDS = FOLLOWERS_IDS.concat(ids);
}

function fetchFollowersIds() {
	// Call the twitter API
	TAPI.get(TwitterResourceURL.userFollowersIds, followers_param, function(err, data, response) {
		handleTwitterReplyError(followers_req, followers_res, err);
		if(data.ids && data.ids.length>0) {
			addFollowersIds(data.ids);
			IDS_COUNT += IDS_COUNT_PER_FETCH;
			if(data.next_cursor>0 && IDS_COUNT < MAX_FOLLOWERS_COUNT){
				followers_param.cursor = data.next_cursor;
				fetchFollowersIds();
			} else if(!FINISHED_FETCHING_FOLLOWERS) {
				FINISHED_FETCHING_FOLLOWERS = true;
				fetchFollowersInfo();
			}
		} else if(!FINISHED_FETCHING_FOLLOWERS) {
			FINISHED_FETCHING_FOLLOWERS = true;
			fetchFollowersInfo();
		}
	});
}

exports.get_influencial_followers = (params, req, res, next) => {
	
	_params = handleUserIdParse(params);
	_params.cursor = -1;
	_params.stringify_ids = true;
	_params.count = IDS_COUNT_PER_FETCH;
	_params.skip_status = true;

	followers_req = req;
	followers_res = res;
	followers_param = _params;
	fetchFollowersIds();
}

/************************************************
*												*
 ======= Task: Get the original tweet ==========
*												*
*************************************************/

function getSingletTweet(params, req, res) {
	// Call the twitter API
	TAPI.get(TwitterResourceURL.singleTweet, params, function(err, data, response) {
		handleTwitterReplyError(req, res, err);
		tweet = {
			id: data.id_str,
			text: data.full_text,
			date: data.created_at,
			retweets_number: data.retweet_count,
			favorite_count: data.favorite_count
		};
		res.json(tweet);
	});
}

exports.get_tweet = (params, req, res, next) => {
	
	_params = {
		id: params.id,
		trim_user: true,
		include_my_retweet: false,
		tweet_mode: "extended"
	}

	getSingletTweet(_params, req, res);
}


/************************************************
*												*
 =========== Twitter Data Collection ===========
*												*
*************************************************/

exports.getFollowersGraphData = (_user_id,res) => {

	// 29873662
	db.followers.aggregate([
		
		{
			$lookup: {
				from: "users",
				/*localField: "follower_id",
				foreignField: "_id",*/
				pipeline : [
		            {'$match'  : { $expr: {  '$gt' : [ '$followers_count', 1000 ]  }} }
		        ],
				/*match: {                         // $match now is matching
			        "$and": [                           // and actually executed against 
			          {                                 // the foreign collection
			            "followers_count": {
			              "$gt": 1000
			            }
			          }
			        ]
			      },*/
				/*pipeline: [
					{ $match:
						{ $expr:
							{$gte: [ "$followers_count", 1000 ]}
						}
					}
				],*/

				as: "follower"
			}
		},
		{
			$unwind: {
				"path": "$follower",
				"preserveNullAndEmptyArrays": true
			}
		},
		{"$project": {
	        // "followers": "$follower.follower",
	        "id": "$follower.id",
	        "name": "$follower.name",
	         "followers_count": "$follower.followers_count",
	    }},
	]).limit(50).sort( { followers_count: -1 }, function(err,data){
		console.log(err);
		console.log(data);
		res.json(data);
	});
}


// get and save all followers of twitter 
exports.getFollowersFromTwitter = (params) => {
	_params = handleUserIdParse(params);
	_params.cursor = -1;
	_params.stringify_ids = true;
	_params.count = 5000;
	_params.skip_status = true;
	
	(function loop(i) {
		new Promise((IterationResolve, reject) => {
			// Call the twitter API
			TAPI.get(TwitterResourceURL.userFollowersIds, _params).catch(function(e){
				console.log("ids limit exceeded");
				date_ob = new Date();
				console.log("waiting started at " + date_ob.getHours() + " " +date_ob.getMinutes());
				setTimeout(function () {
		            console.log("this message is from iteration waiting");
		            // clear the promise of the current chunk after waiting
		            // for twitter api to cool down
	    			IterationResolve();
		        }, 900000);
				console.log("waiting done");
			}).then(function(data) {
				console.log("fetched ids");
				if(data && data.resp.statusCode==200) {
					console.log("code is 200");
					_params.cursor = data.data.next_cursor;
					if(data.data.ids && data.data.ids.length>0) {

						datalen = Math.ceil(data.data.ids.length/parseFloat(100));
						// chunk the list of 5000 ids into chunks of 100
						Lodash.chunk(data.data.ids,100).reduce( (p, ids, idx) => 
						    p.then(_ =>
						    	// run a promise for each chunk to get user info from the 100 ids
							    new Promise(ChunkingResolve => TAPI.get(TwitterResourceURL.userLookup, {user_id: ids}).catch(function(err){
							    	console.log("chunking limit exceeded");
						    		console.log("waiting");
						    		setTimeout(function () {
							            console.log("this message is from chunking waiting");
							            // clear the promise of the current chunk after waiting
							            // for twitter api to cool down
							            if(idx == datalen)
						    				IterationResolve();
							            ChunkingResolve();
							        }, 900000);
						    		console.log("waiting done");
							    }).then(function(dat, resp) {
								    	console.log("inside chunking");
								    	if(dat && dat.resp.statusCode==200) {
								    		console.log("chunking code 200");
								    		console.log("saving user count"+dat.data.length);
								    		for(user of dat.data) {
								    			user['_id'] = user.id;
								    			// save user infos to the database
								    			db.users.save(user,function(err,tweet){
								    				
								    			});
								    		}
								    		// clear the promise of the current chunk
								    		console.log(idx, datalen-1);
								    		if(idx == datalen-1)
								    			IterationResolve();
								    		ChunkingResolve();
								    	} else {
								    		console.log("This is an unpredicted situation from inside chinking");
								    		// clear current promise chunk anyway
								    		if(idx == datalen)
								    			IterationResolve();
								    		ChunkingResolve();
								    	}
									})
							    )
							)
						// After the end of every chunk processing, proceed to the next chunk
						, Promise.resolve() );
					}
				}
			});
		}).then(loop.bind(null, i+1));
	})(0);
}